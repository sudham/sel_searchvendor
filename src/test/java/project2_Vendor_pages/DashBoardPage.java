package project2_Vendor_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashBoardPage extends ProjectMethods {
	
	public DashBoardPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	

	@FindBy(how=How.XPATH,using="(//button[@class='btn btn-default btn-lg'])[4]") WebElement eleMoveToVendor;
	public DashBoardPage moveElementVendor() {
		Actions builder=new Actions(driver);
			builder.moveToElement(eleMoveToVendor).perform();
			return this;
		}

	@FindBy(how=How.XPATH,using="//a[contains(text(),'Search for Vendor')]") WebElement searchVendor;
	public VendorSearch clickSearchVendor() {
		Actions builder=new Actions(driver);
			builder.click(searchVendor).perform();
			return new VendorSearch();
		}


}
