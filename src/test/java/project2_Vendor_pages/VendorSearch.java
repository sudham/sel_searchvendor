package project2_Vendor_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearch extends ProjectMethods {
	
	public VendorSearch() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID,using="vendorTaxID") WebElement eleVendor;
	public VendorSearch enterTaxid(String vendor) {
		clearAndType(eleVendor, vendor);
		return this;
	}
	
	@FindBy(how=How.ID,using="buttonSearch") WebElement elebuttonSearch;
	public VendorSearch buttonSearch() {
		click(elebuttonSearch);
		return this;
	}
	
	public VendorSearch selectVendorName() {
		WebElement table = driver.findElementByXPath("//table[@class='table']");
		List<WebElement> row= table.findElements(By.tagName("tr"));
		WebElement eachrow=row.get(1);
		List<WebElement> column = eachrow.findElements(By.tagName("td"));
		String vendorName = column.get(0).getText();
		System.out.println("Vendore Name is"+vendorName);
		return this;
	
		
	}

}
