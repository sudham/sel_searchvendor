package project2_Vendor_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPageVendorSearch extends ProjectMethods {
	
	public LoginPageVendorSearch() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.ID,using="email") WebElement eleEmail;
	public LoginPageVendorSearch enterEmail(String emailId) {
			clearAndType(eleEmail, emailId);
			return this;
		}
	
	@FindBy(how=How.ID,using="password") WebElement elePassword;
	public LoginPageVendorSearch enterPassword(String password) {
			clearAndType(elePassword, password);
			return this;
		}
	
	@FindBy(how=How.ID,using="buttonLogin") WebElement elesubmit;
public DashBoardPage loginSubmit() {
	click(elesubmit);
	return new DashBoardPage();
	
		
	}

}
