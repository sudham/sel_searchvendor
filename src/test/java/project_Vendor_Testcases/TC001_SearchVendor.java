package project_Vendor_Testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadsPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyleadsPage;
import com.framework.pages.ViewLeadPage;

import project2_Vendor_pages.LoginPageVendorSearch;

public class TC001_SearchVendor extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_searchVendor";
		testDescription = "searchVendor";
		testNodes = "leads";
		author = "Sudha";
		category = "smoke";
		dataSheetName = "TC004";

	}
	
	
    @Test(dataProvider = "fetchData")
	public void searchVendor(String emailId, String password, String vendor) {
		new LoginPageVendorSearch()
		.enterEmail(emailId)
		.enterPassword(password)
		.loginSubmit()
		.moveElementVendor()
		.clickSearchVendor()
		.enterTaxid(vendor)
		.buttonSearch()
		.selectVendorName();
		
		
		

	}
	


}
